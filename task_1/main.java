package homework.task_1;

import homework.task_1.Animals.*;

public class main {
    public static void main(String[] args) {

        Animals cat = new Animals("Марсик",5);

        System.out.println(cat.getName());
        System.out.println(cat.getAge());
        cat.getStatus();

        Cat murzik = new Cat("Мурзик", 2);

        System.out.println();
        System.out.println(murzik.getName());
        System.out.println(murzik.getVoice());
        murzik.getStatus();

        Dog lord = new Dog("Лорд",5);
        System.out.println();
        System.out.println(lord.getName());
        System.out.println(lord.getVoice());
        lord.getStatus();

        cat.setName("Васька");
        murzik.setName("Барсик");
        lord.setName("Пушок");

        System.out.println(cat.getName());
        System.out.println(cat.getAge());
        System.out.println(murzik.getName());
        System.out.println(murzik.getAge());
        System.out.println(lord.getName());
    }
}
