package homework.task_1.Animals;

import homework.task_1.Randomizer;

public class Dog extends Animals {
    private String voice = "Гав-гав";

    public Dog(String name, int age) {
        super(name, age);
    }
    public String getVoice() {
        return voice;
    }
}
