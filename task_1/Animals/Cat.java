package homework.task_1.Animals;

import homework.task_1.Randomizer;

public class Cat extends Animals {
    private String voice = "Муррр";

    public Cat(String name, int age) {
        super(name, age);
    }
    public String getVoice() {
        return voice;
    }
}